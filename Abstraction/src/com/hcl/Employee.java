package com.hcl;

public class Employee {

	private int id; // data hiding
	private String name;
	private double salary;
	
	public Employee() {
		
		super();
		
	}
	

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getSalary() {
		return salary;
	}

	public void setSalary(double salary) {
		this.salary = salary;
	}


	@Override
	public String toString() {
		return "Employee [id=" + id + ", name=" + name + ", salary=" + salary + "]";
	}


	
	


	@Override
	public int hashCode() {
		
		return this.id;
	}


	

	
	/*
	 * public String toString() {
	 * 
	 * return this.id+" "+this.name;
	 * 
	 * 
	 * }
	 */
	
	
	  @Override public boolean equals(Object obj) {
	  
	  boolean flag = false;
	  
	  Employee emp = (Employee) obj;
	  
	  if(this.id == emp.id) {
	  
	  flag = true;
	  
	  }
	  
	  return flag;
	  
	  }
	 
	
	
	
	
}
