package com.hcl.oops;
 

public class Student  {
	public Student( ) {
		System.out.println("Default Constructor");
	}
	public Student (int id,String name) {
		
		System.out.println("parameter Constructor");
		System.out.println("Id " +id);
		System.out.println("Name "+name);
	}
	
	
	
	public static void main(String[] args) {
		
		Student student1 =   new  Student(111, "nishi");
		Student student2 = new Student (123, "mouni");
		
		System.out.println(student1);
		
		hello();

	}

	public  static void hello() {
			
		System.out.println("hello()");
	}
	
	
	

}
