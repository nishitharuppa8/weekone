package com.hcl.controls;

public class StringExample {

	public static void main(String[] args) {
		
		
		String str1 = "Java";
		String str2 = new String("Nishi");
		
		System.out.println(str1.length());
		
		System.out.println(str1.charAt(3));
		
		System.out.println(str2.toUpperCase());

	}

}
