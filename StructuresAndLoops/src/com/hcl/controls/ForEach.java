package com.hcl.controls;

public class ForEach {
	
	public static void main(String[] args) {
		
		int arr[] = {2,4,6,8,10,12}; 
		for (int i = 0;  i < arr.length; i++) {
			System.out.println(arr[i]);
		}
		
	
		System.out.println();
		String s[] = new String[4];
		s[0] = "nishi";
		s[1] = "mani";
		s[2] = "mouni";
		s[3] = "naidu";
		  
		for (int i= 0; i < s.length; i ++)
		{
			System.out.println(s[i]);
		}
		
	}

}
