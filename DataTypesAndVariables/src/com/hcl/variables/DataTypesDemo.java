package com.hcl.variables;

public class DataTypesDemo {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		byte b = 6;
		short s = 5;
		char c = 'A';
		int i = 5;
		float f = 2.55f;
		long l = 50000l;
		double d = 5.55;
		boolean bValue = true;

		System.out.println("Byte " + b);
		System.out.println("Short " + s);
		System.out.println("char " + c);
		System.out.println("Int " + i);
		System.out.println("Float " + f);
		System.out.println("Long " + l);
		System.out.println("Double " + d);
		System.out.println("Boolean " + bValue);
	}

}
