package com.hcl.variables;

public class TypeCastingDemo {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		byte b1 = 40;

		int i1 = (int) b1;
		int i2 = 50;
		byte b2 = (byte) i2;
		System.out.println(i1);
		System.out.println(b2);
		System.out.println("byte value " + b1);
		
		long l = 9999L;
		float f = 4;
		System.out.println();

	}

}
