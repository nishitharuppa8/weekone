package com.hcl.inherit;

public class Parent  extends Object{
	
	int pid = 100;
		
	public Parent() {
		super();
		
	}
	
	protected Object methodOne() {
		
		
		System.out.println("m1() from parent...");
		
		return null;
		
	}
	
	public  String  toString() { 
		
		
		return "Parent class";
		
	}
	
	
	

}
