package com.hcl.inherit;

public class Test {

	public static void main(String[] args) {

		Parent p = new Parent();

		System.out.println(p.pid);

		p.methodOne();

		System.out.println(p.toString());

		String s = new String("nishi");

		System.out.println(s.toString());

		Integer i = new Integer(55);

		System.out.println(i.toString());

		Child c = new Child();

		System.out.println(c.cid);
		System.out.println(c.pid);

		c.methodOne();

		Object obj = new Object();

		obj = new Parent();

		obj = new Child();

		Parent p1 = new Parent();

		p1 = new Child();

	}

}
