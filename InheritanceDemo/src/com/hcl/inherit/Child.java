package com.hcl.inherit;

public class Child extends Parent {
	
	int cid = 45;
	
	
	public Child() {
		
		super();
		
	}
	public String methodOne() { 
		System.out.println("m1() from child");
		
		return null;
		
	}
	
	
	public void methodTwo() {
		
		
		System.out.println("m2() from child..");
		
	}

}
