package com.hcl.day3;

class ThreeDimensionShape{
	double width,depth,height;
	ThreeDimensionShape(){
		width=height=depth=0;
	}
	ThreeDimensionShape(double length){
		width=height=depth=length;
	}
	ThreeDimensionShape(double a,double b,double c){
		width=a;
		depth=b;
		height=c;
	}
     double  volume() {
    	 return width * depth * height;
     }
}


public class Volume {

	public static void main(String[] args) {
		ThreeDimensionShape shape1=new ThreeDimensionShape();
		ThreeDimensionShape shape2=new ThreeDimensionShape(6);
		ThreeDimensionShape shape3=new ThreeDimensionShape(2,3,5);
		double volume;
	    volume=shape1.volume();
	    System.out.println("Volume of Shape1 is " + volume);
		volume=shape2.volume();
		System.out.println("Volume of Shape2 is " + volume);
		volume=shape3.volume();
		System.out.println("Volume of shape3 is " + volume);	
	
    

	}

}
