package com.hcl.day3;
 class Vehicle{
		void noOfWheels() {
			System.out.println("No of wheels undefined");
		}
	}
		class Scooter extends Vehicle{
			void noOfWheels() {
				System.out.println("No of wheels 2");
			}
			
		}
		class Car extends Vehicle {
			void noOfWheels() {
				System.out.println("No of wheels 4");
			}
		
}

 class main {

	public main(String[] args) {
		
	 Vehicle a=new Vehicle(); 
	 a.noOfWheels(); 
	 Scooter b=new Scooter();
	 b.noOfWheels();
	Car d=new Car();
	d.noOfWheels();
		

	}

}

