package com.hcl.inputs;

import java.util.Scanner;
public class ReadInputs {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter  A value");
		
		String input1 = scanner.next();
		  scanner.nextLine(); //TO BREAK THE DATA FLOW OR TO HANDLE NEW LINE
		System.out.println(" A = " +input1);
		  
		
		System.out.println("Enter B value");
		String input2 = scanner.nextLine();
		System.out.println(" B = "+ input2);
		
		

	}

}
