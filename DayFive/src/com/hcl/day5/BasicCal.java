package com.hcl.day5;
import java.util.Scanner;

public class BasicCal {

    public static void main(String[] args) {

    	int  number1, number2;
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter First number:");

       
        number1 = scanner.nextInt();
        System.out.print("Enter Second number:");
        number2 = scanner.nextInt();

        System.out.print("Enter an operator (+, -, *, /): ");
        char operator = scanner.next().charAt(0);

        scanner.close();
          int output;

        switch(operator)
        {
            case '+':
            	output = number1 + number2;
                break;

            case '-':
            	output = number1 - number2;
                break;

            case '*':
            	output = number1 * number2;
                break;

            case '/':
            	output = number1 / number2;
                break;

            
            default:
                System.out.printf("You have entered wrong operator");
                return;
        }

        System.out.println(number1+" "+operator+" "+number2+": "+output);
    }
}