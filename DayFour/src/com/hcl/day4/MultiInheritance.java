package com.hcl.day4;

public class MultiInheritance{
	String customerName;
	int accountNo;
	public MultiInheritance(String a,int b) {
		a = customerName;
		b = accountNo;
	}
	void display() {
		System.out.println("Customer Name:"+customerName);
		System.out.println("Account Number:"+accountNo);
	}
}	
	class CurrentAccount extends MultiInheritance{
		int currentBal;
	    public CurrentAccount(String a, int b, int c){
	    	super(a,b);
	        currentBal=c;
	    }
	    void display() {
	                super.display();
	                System.out.println ("Current Balance:"+currentBal);
	    }
}
	
	class AccountDetails extends CurrentAccount{
		int deposit, withdrawal;
	    	public AccountDetails(String a, int b, int c,int d,int e) {
			super(a, b, c);
			deposit = d;
			withdrawal = e;
		}
	    	void display(){
	               super.display();
	               System.out.println ("Deposit:"+deposit);
	               System.out.println ("Withdrawals:"+withdrawal);
	    }	    	
}
class Inheritance{
	public static void main(String[] args) {
		AccountDetails Account = new AccountDetails("Nishi",5555,12000,3000,1000);
		Account.display();
	}
}

