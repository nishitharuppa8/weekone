package com.hcl.day4;
interface Common{
	abstract String markAttendance();
	abstract String dailyTask();
	abstract String displayDetails();
}
class Employee implements Common{
	public String markAttendance() {
		return "Attendance marked for today";
	}
	public String dailyTask() {
		return "complete coding of module1";
	}
	public String displayDetails() {
		return "employee details";
	}
	
}
class Manager implements Common{
	public String markAttendance() {
		return "Attendance marked for today";
	}
	public String dailyTask() {
		return "create project architecture";
	}
	public String displayDetails() {
		return "manager details";
	}
}


 class main {
	public static void main(String[] args) {
		Common employee=new Employee();
		Common manager=new Manager();
		System.out.println(employee.markAttendance());
		System.out.println(employee.dailyTask());
		System.out.println(employee.displayDetails());
		System.out.println(manager.markAttendance());
		System.out.println(manager.dailyTask());
		System.out.println(manager.displayDetails());	
}
}
